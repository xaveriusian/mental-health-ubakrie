import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Text, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontAwesome } from '@expo/vector-icons';
import colors from '../colors';
import { Entypo } from '@expo/vector-icons';
import { Card, Paragraph } from 'react-native-paper';
import Collapsible from 'react-native-collapsible';

const catImageUrl = "https://i.guim.co.uk/img/media/26392d05302e02f7bf4eb143bb84c8097d09144b/446_167_3683_2210/master/3683.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=49ed3252c0b2ffb49cf8b508892e452d";



const data = [
  {
    id: 1,
    name: 'Doctor 1',
    specialty: 'Cardiologist',
    hospital: 'Hospital A',
  },
  {
    id: 2,
    name: 'Doctor 2',
    specialty: 'Neurologist',
    hospital: 'Hospital B',
  },
  // ...
];

const DoctorList = () => {
  const [activeSections, setActiveSections] = useState([]);

  const toggleAccordion = (index) => {
    const newSections = [...activeSections];
    const sectionIndex = newSections.indexOf(index);

    if (sectionIndex > -1) {
      newSections.splice(sectionIndex, 1);
    } else {
      newSections.push(index);
    }

    setActiveSections(newSections);
  };

  return (
    <View style={styles.container}>
      {data.map((item, index) => (
        <View key={item.id}>
          <TouchableOpacity onPress={() => toggleAccordion(index)}>
            <Text style={styles.doctorName}>{item.name}</Text>
          </TouchableOpacity>
          <Collapsible collapsed={activeSections.indexOf(index) === -1}>
            <Card>
              <Card.Content>
                <Paragraph>Specialty: {item.specialty}</Paragraph>
                <Paragraph>Hospital: {item.hospital}</Paragraph>
              </Card.Content>
            </Card>
          </Collapsible>
        </View>
      ))}
    </View>
  );
};

const Home = () => {
  const navigation = useNavigation();

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <FontAwesome name="search" size={24} color={colors.gray} style={{marginLeft: 15}}/>
      ),
      headerRight: () => (
        <Image
          source={{ uri: catImageUrl }}
          style={{
            width: 40,
            height: 40,
            marginRight: 15,
          }}
        />
      ),
    });
  }, [navigation]);

  return (
    <View style={styles.container}>
      <DoctorList />
      <TouchableOpacity
        onPress={() => navigation.navigate("Chat")}
        style={styles.chatButton}
      >
        <Entypo name="chat" size={24} color={colors.lightGray} />
      </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    backgroundColor: "#fff",
  },
  chatButton: {
    backgroundColor: colors.primary,
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: colors.primary,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: .9,
    shadowRadius: 8,
    marginRight: 20,
    marginBottom: 50,
  },
  doctorName: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
