import * as React from "react";
import { Image } from "expo-image";
import { StyleSheet, Text, View } from "react-native";
import { FontFamily } from "../GlobalStyles";

const Splash = () => {
  return (
    <View style={styles.splash}>
      <Image
        style={styles.oberlyIcon}
        contentFit="cover"
        source={require("../assets/img/oberly@2x.png")}
      />
      <View style={styles.logo}>
        <Image
          style={styles.groupIcon}
          contentFit="cover"
          source={require("../assets/img/group@2x.png")}
        />
        <Text style={styles.somdoctor}>Mental Health</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  oberlyIcon: {
    top: -86,
    left: -64,
    width: 597,
    height: 1018,
    position: "absolute",
  },
  groupIcon: {
    height: "61.97%",
    width: "42.59%",
    top: "0%",
    right: "29.06%",
    bottom: "38.03%",
    left: "28.35%",
    maxWidth: "100%",
    maxHeight: "100%",
    position: "absolute",
    overflow: "hidden",
  },
  somdoctor: {
    top: 187,
    left: 0,
    fontSize: 36,
    letterSpacing: 0,
    lineHeight: 22,
    fontWeight: "500",
    fontFamily: FontFamily.poppinsMedium,
    color: "#fff",
    textAlign: "left",
    width: 261,
    height: 62,
    position: "absolute",
  },
  logo: {
    height: "30.67%",
    width: "69.6%",
    top: "28.69%",
    right: "14.93%",
    bottom: "40.64%",
    left: "15.47%",
    position: "absolute",
  },
  splash: {
    borderRadius: 30,
    backgroundColor: "#18a0fb",
    flex: 1,
    width: "100%",
    height: 812,
    overflow: "hidden",
  },
});

export default Splash;
