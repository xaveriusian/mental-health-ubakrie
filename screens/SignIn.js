import * as React from "react";
import { Text, StyleSheet, View } from "react-native";
import { Image } from "expo-image";
import { FontFamily, Color, Border, FontSize } from "../GlobalStyles";

const SignIn = () => {
  return (
    <View style={styles.signIn}>
      <Text style={[styles.dontHaveAnContainer, styles.timeFlexBox]}>
        <Text
          style={[styles.dontHaveAn, styles.name1Typo]}
        >{`Don’t have an account? `}</Text>
        <Text style={styles.signIn1Typo}>Sign In</Text>
      </Text>
      <View style={[styles.signIn2, styles.nameLayout]}>
        <View style={[styles.signInChild, styles.childLayout]} />
        <Text style={[styles.signIn3, styles.name1FlexBox]}>Sign In</Text>
      </View>
      <Text style={[styles.forgotPassword, styles.signIn1Typo]}>
        Forgot Password
      </Text>
      <View style={[styles.password, styles.nameLayout]}>
        <View style={[styles.passwordChild, styles.childLayout]} />
        <View style={[styles.passwordParent, styles.name1Position]}>
          <Text style={[styles.password1, styles.name1FlexBox]}>Password</Text>
          <Image
            style={styles.vectorIcon}
            contentFit="cover"
            source={require("../assets/vector.png")}
          />
        </View>
      </View>
      <View style={[styles.name, styles.nameLayout]}>
        <View style={[styles.passwordChild, styles.childLayout]} />
        <Text style={[styles.name1, styles.name1Position]}>Name</Text>
      </View>
      <Text style={[styles.welcome, styles.name1FlexBox]}>Welcome!</Text>
      <Text style={styles.letsSignIn}>Let’s Sign In</Text>
      <Image
        style={[styles.backIcon, styles.iconLayout]}
        contentFit="cover"
        source={require("../assets/back.png")}
      />
      <View style={[styles.barsStatusBarIphoneL, styles.password1Position]}>
        <View style={styles.battery}>
          <View style={[styles.border, styles.timePosition]} />
          <Image
            style={[styles.capIcon, styles.iconLayout]}
            contentFit="cover"
            source={require("../assets/cap.png")}
          />
          <View style={styles.capacity} />
        </View>
        <Image
          style={styles.wifiIcon}
          contentFit="cover"
          source={require("../assets/wifi.png")}
        />
        <Image
          style={styles.cellularConnectionIcon}
          contentFit="cover"
          source={require("../assets/cellular-connection.png")}
        />
        <View style={styles.timeStyle}>
          <Text style={[styles.time, styles.timePosition]}>9:41</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  timeFlexBox: {
    textAlign: "center",
    letterSpacing: 0,
  },
  name1Typo: {
    fontFamily: FontFamily.poppinsRegular,
    color: Color.colorGray,
  },
  nameLayout: {
    height: 52,
    width: 325,
    left: 25,
    position: "absolute",
  },
  childLayout: {
    borderRadius: Border.br_3xs,
    left: 0,
    top: 0,
    height: 52,
    width: 325,
    position: "absolute",
  },
  name1FlexBox: {
    textAlign: "left",
    letterSpacing: 0,
  },
  signIn1Typo: {
    color: Color.colorCornflowerblue,
    fontFamily: FontFamily.poppinsMedium,
    fontWeight: "500",
  },
  name1Position: {
    left: 19,
    top: 16,
    position: "absolute",
  },
  iconLayout: {
    maxHeight: "100%",
    maxWidth: "100%",
    position: "absolute",
    overflow: "hidden",
  },
  password1Position: {
    left: 0,
    top: 0,
    position: "absolute",
  },
  timePosition: {
    left: "0%",
    position: "absolute",
  },
  dontHaveAn: {
    color: Color.colorGray,
  },
  dontHaveAnContainer: {
    top: 571,
    left: 90,
    lineHeight: 15,
    fontSize: FontSize.size_smi,
    textAlign: "center",
    position: "absolute",
  },
  signInChild: {
    backgroundColor: Color.colorCornflowerblue,
  },
  signIn3: {
    left: 137,
    color: Color.colorWhite,
    lineHeight: 20,
    fontSize: FontSize.sFSubheadlineSemibold_size,
    top: 16,
    textAlign: "left",
    fontFamily: FontFamily.poppinsMedium,
    fontWeight: "500",
    position: "absolute",
  },
  signIn2: {
    top: 504,
  },
  forgotPassword: {
    top: 401,
    left: 244,
    lineHeight: 28,
    textAlign: "right",
    letterSpacing: 0,
    color: Color.colorCornflowerblue,
    fontSize: FontSize.size_smi,
    position: "absolute",
  },
  passwordChild: {
    backgroundColor: Color.colorWhite,
  },
  password1: {
    lineHeight: 20,
    fontSize: FontSize.sFSubheadlineSemibold_size,
    left: 0,
    top: 0,
    position: "absolute",
    color: Color.colorGray,
    fontFamily: FontFamily.poppinsRegular,
  },
  vectorIcon: {
    top: 5,
    left: 274,
    width: 12,
    height: 10,
    position: "absolute",
  },
  passwordParent: {
    width: 286,
    height: 20,
  },
  password: {
    top: 345,
  },
  name1: {
    textAlign: "left",
    letterSpacing: 0,
    lineHeight: 20,
    fontSize: FontSize.sFSubheadlineSemibold_size,
    color: Color.colorGray,
    fontFamily: FontFamily.poppinsRegular,
  },
  name: {
    top: 273,
  },
  welcome: {
    top: 146,
    fontSize: 30,
    lineHeight: 39,
    left: 25,
    textAlign: "left",
    fontFamily: FontFamily.poppinsMedium,
    fontWeight: "500",
    color: Color.colorGray,
    position: "absolute",
  },
  letsSignIn: {
    top: 54,
    left: 135,
    fontSize: 18,
    lineHeight: 40,
    fontFamily: FontFamily.poppinsMedium,
    fontWeight: "500",
    color: Color.colorGray,
    textAlign: "center",
    position: "absolute",
  },
  backIcon: {
    height: "5.63%",
    width: "12.2%",
    top: "6.28%",
    right: "81.13%",
    bottom: "88.08%",
    left: "6.67%",
  },
  border: {
    height: "100%",
    width: "90.43%",
    top: "0%",
    right: "9.57%",
    bottom: "0%",
    borderRadius: 3,
    borderStyle: "solid",
    borderColor: Color.colorGray,
    borderWidth: 1,
    opacity: 0.35,
  },
  capIcon: {
    height: "35.29%",
    width: "5.46%",
    top: "32.35%",
    right: "0%",
    bottom: "32.35%",
    left: "94.54%",
    opacity: 0.4,
  },
  capacity: {
    height: "64.71%",
    width: "73.99%",
    top: "17.65%",
    right: "17.79%",
    bottom: "17.65%",
    left: "8.22%",
    borderRadius: 1,
    backgroundColor: Color.colorGray,
    position: "absolute",
  },
  battery: {
    height: "25.76%",
    width: "6.49%",
    top: "39.39%",
    right: "3.82%",
    bottom: "34.85%",
    left: "89.69%",
    position: "absolute",
  },
  wifiIcon: {
    width: 15,
    height: 11,
  },
  cellularConnectionIcon: {
    width: 17,
    height: 11,
  },
  time: {
    marginTop: -4.5,
    top: "50%",
    fontWeight: "600",
    fontFamily: FontFamily.sFSubheadlineSemibold,
    lineHeight: 20,
    fontSize: FontSize.sFSubheadlineSemibold_size,
    color: Color.colorGray,
    textAlign: "center",
    letterSpacing: 0,
    width: "100%",
    left: "0%",
  },
  timeStyle: {
    height: "47.73%",
    width: "14.4%",
    top: "15.91%",
    right: "80%",
    bottom: "36.36%",
    left: "5.6%",
    position: "absolute",
  },
  barsStatusBarIphoneL: {
    right: 0,
    height: 44,
  },
  signIn: {
    borderRadius: 30,
    backgroundColor: "#f9f9f9",
    flex: 1,
    height: 812,
    overflow: "hidden",
    width: "100%",
  },
});

export default SignIn;
